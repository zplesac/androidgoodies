package co.infinum.androidgoodies;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.Minutes;
import org.joda.time.Seconds;
import org.joda.time.format.ISODateTimeFormat;

import java.util.Date;

/**
 * Created by zeljkoplesac on 04/02/15.
 */
public class DateUtils {

    /**
     * Converts GMT String to DateTime
     * @param gmtTime GMT String to parse
     * @return Parsed Strign as Datetime
     */
    public static DateTime isoToDateTime(String gmtTime) {

        try{
            DateTime dt = ISODateTimeFormat.dateTime().parseDateTime(gmtTime);
            return dt;

        }
        catch (Exception e){
            e.printStackTrace();
            return new DateTime();
        }
    }


    /**
     * Converts GMT String to Date
     * @param gmtTime GMT String to parse
     * @return Parsed Strign as Date
     */
    public static Date isoToDate(String gmtTime) {
        try{
            DateTime dt = ISODateTimeFormat.dateTime().parseDateTime(gmtTime);
            return dt.toDate();

        }
        catch (Exception e){
            e.printStackTrace();
            return new Date();
        }
    }

    public static int getDifferenceInDays(Date startDate, Date endDate) {
        DateTime start = new DateTime(startDate);
        DateTime end = new DateTime(endDate);

        return Days.daysBetween(start.withTimeAtStartOfDay(), end.withTimeAtStartOfDay()).getDays();
    }

    public static int getDifferenceInMinutes(Date startDate, Date endDate) {
        DateTime start = new DateTime(startDate);
        DateTime end = new DateTime(endDate);

        return Minutes.minutesBetween(end, start).getMinutes();
    }

    public static int getDifferenceInSeconds(Date startDate, Date endDate) {
        DateTime start = new DateTime(startDate);
        DateTime end = new DateTime(endDate);

        return Seconds.secondsBetween(end, start).getSeconds();
    }
}
