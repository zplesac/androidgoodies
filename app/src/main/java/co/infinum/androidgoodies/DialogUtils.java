package co.infinum.androidgoodies;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.widget.Toast;

/**
 * Created by zeljkoplesac on 04/02/15.
 */
public class DialogUtils {

    /**
     * Heleper method to show native Android build dialog with OK options as only button
     * @param activity Activity which is used to show alert dialog
     * @param title Alert dialog title
     * @param message Message to show in alert dialog
     */
    public static void showAlertDialog(Activity activity, String title, String message){
        final AlertDialog.Builder builder = new AlertDialog.Builder(activity);

        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);
        builder.setPositiveButton(activity.getString(R.string.ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(final DialogInterface dialog, final int id) {
                dialog.cancel();
            }
        });
        final AlertDialog alert = builder.create();

        if(!activity.isFinishing()){
            alert.show();
        }
    }

    /**
     * Heleper method to show native Android build dialog with OK options as only button
     * @param activity Activity which is used to show alert dialog
     * @param title Alert dialog title
     * @param message Message to show in alert dialog
     */
    public static void showAlertDialog(Activity activity, String title, String message, DialogInterface.OnClickListener listener){
        final AlertDialog.Builder builder = new AlertDialog.Builder(activity);

        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);
        builder.setPositiveButton(activity.getString(R.string.ok), listener);
        final AlertDialog alert = builder.create();

        if(!activity.isFinishing()){
            alert.show();
        }
    }

    /**
     * Show a dialog when Google Play Services are outdated or unavailable.
     *
     * @param activity
     */
    public static void showGooglePlayServicesUnavailableDialog(final Activity activity, final String title, String message,
                                                                    String positiveButtonText, String negativeButtonText,
                                                                    final String googlePlayServicesAreNeccesaryText) {
        AlertDialog.Builder builder = new AlertDialog.Builder(activity);

        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);
        builder.setPositiveButton(positiveButtonText, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                try {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.google.android.gms"));
                    activity.startActivity(intent);
                } catch (ActivityNotFoundException e) {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.google.android.gms&hl=en"));
                    activity.startActivity(intent);
                }

                activity.finish();
            }
        });

        builder.setNegativeButton(negativeButtonText, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                // dialog.cancel();
                showGooglePlayIsNeccesary(activity, title, googlePlayServicesAreNeccesaryText);
            }
        });

        final AlertDialog alert = builder.create();

        if(!activity.isFinishing()){
            alert.show();
        }
    }

    /**
     * Show a dialog when Google Play Services are necessary for application.
     *
     * @param activity
     */
    private static void showGooglePlayIsNeccesary(final Activity activity, String title, String googlePlayServicesAreNeccesaryText) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(activity);

        builder.setTitle(title).setMessage(googlePlayServicesAreNeccesaryText).setCancelable(false)
                .setPositiveButton(activity.getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                        activity.finish();
                    }
                });
        final AlertDialog alert = builder.create();

        if(!activity.isFinishing()){
            alert.show();
        }
    }

    /**
     * Shows a long time duration toast message.
     *
     * @param msg Message to be show in the toast.
     * @return Toast object just shown
     * **
     */
    public static Toast showToast(Context ctx, CharSequence msg) {
        return showToast(ctx, msg, Toast.LENGTH_LONG);
    }

    /**
     * Shows the message passed in the parameter in the Toast.
     *
     * @param msg      Message to be show in the toast.
     * @param duration Duration in milliseconds for which the toast should be shown
     * @return Toast object just shown
     * **
     */
    public static Toast showToast(Context ctx, CharSequence msg, int duration) {
        Toast toast = Toast.makeText(ctx, msg, Toast.LENGTH_SHORT);
        toast.setDuration(duration);
        toast.show();
        return toast;
    }
}
