package co.infinum.androidgoodies;

import android.content.Context;
import android.util.DisplayMetrics;

/**
 * Created by zeljkoplesac on 14/02/15.
 */
public class DisplayUtils {

    // display width in pixels
    public static int getDisplayWidth(Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return displayMetrics.widthPixels;
    }

    // display height in pixels
    public static int getDisplayHeight(Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return displayMetrics.heightPixels;
    }
}
