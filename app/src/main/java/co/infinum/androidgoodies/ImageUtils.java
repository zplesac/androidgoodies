package co.infinum.androidgoodies;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Environment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by zeljkoplesac on 04/02/15.
 */
public class ImageUtils {

    public static final int INVALID_ORIENTATION = -1;


    /*

    /**
     * Create a file Uri for saving an image
    public static Uri getOutputMediaFileUri(Activity activity) {
        return Uri.fromFile(getOutputMediaFile(activity));
    }

    /**
     * Create a File for saving an image or video

    private static File getOutputMediaFile(Activity activity) {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.

        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), activity.getString(R.string.app_name));
        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d("ERROR", "failed to create directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                "IMG_" + timeStamp + ".jpg");

        return mediaFile;
    }*/

    /**
      This method convets dp unit to equivalent device specific value in
    * pixels.
    *
            * @param dp A value in dp(Device independent pixels) unit. Which we need
    *           to convert into pixels
    * @return An int value to represent Pixels equivalent to dp according to
    * device
    */
    public static int convertDpToPixels(int dp, Context context) {
        float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
                context.getResources().getDisplayMetrics());
        return Math.round(px);
    }

    /**
     * This methods rotates picture to default phone orientation
     * @param picture Picture file
     * @return
     * @throws NullPointerException
     */
    public static Bitmap rotatePicture(File picture, int angle) throws NullPointerException {
        // Declare
        FileInputStream inStream = null;
        // Create options
        BitmapFactory.Options options = new BitmapFactory.Options();

        Matrix matrix = new Matrix();
        matrix.postRotate(angle);

        // Increment inSampleSize progressively to reduce image resolution and size. If
        // the program is properly managing memory, and you don't have other large images
        // loaded in memory, this loop will generally not need to go through more than 3
        // iterations. To be safe though, we stop looping after a certain amount of tries
        // to avoid infinite loops

        for (options.inSampleSize = 1; options.inSampleSize <= 32; options.inSampleSize++) {

            try {
                // Load the bitmap from file
                inStream = new FileInputStream(picture);

                Bitmap originalBitmap = BitmapFactory.decodeStream(inStream, null, options);

                // Rotate the bitmap
                Bitmap rotatedBitmap = Bitmap.createBitmap(originalBitmap, 0, 0, originalBitmap.getWidth(), originalBitmap.getHeight(), matrix, true);
                return rotatedBitmap;
            } catch (OutOfMemoryError e) {
                e.printStackTrace();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }

        throw new IllegalStateException();
    }

    /***
     * Gets the orientation of the image pointed to by the parameter uri
     *
     * @return Image orientation value corresponding to <code>ExifInterface.ORIENTATION_*</code> <br/>
     *         Returns -1 if file is not found or data is corrupted.
     ****/
    public static int getImageOrientation(File file) {

        ExifInterface ei;
        try {
            ei = new ExifInterface(file.getPath());
            return ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, INVALID_ORIENTATION);
        } catch (IOException e) {
            e.printStackTrace();
            return 0;
        }
    }

    /**
     * Converts the passed in drawable to Bitmap representation
     *
     * @throws NullPointerException If the parameter drawable is null.
     *
     */
    public static Bitmap drawableToBitmap(Drawable drawable) {

        if (drawable == null) {
            throw new NullPointerException("Drawable to convert should NOT be null");
        }

        if (drawable instanceof BitmapDrawable) {
            return ((BitmapDrawable) drawable).getBitmap();
        }

        if (drawable.getIntrinsicWidth() <= 0 && drawable.getIntrinsicHeight() <= 0) {
            return null;
        }

        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);

        return bitmap;
    }

    // Scale and maintain aspect ratio given a desired width
    // BitmapScaler.scaleToFitWidth(bitmap, 100);
    public static Bitmap scaleToFitWidth(Bitmap b, int width)
    {
        float factor = width / (float) b.getWidth();
        return Bitmap.createScaledBitmap(b, width, (int) (b.getHeight() * factor), true);
    }


    // Scale and maintain aspect ratio given a desired height
    // BitmapScaler.scaleToFitHeight(bitmap, 100);
    public static Bitmap scaleToFitHeight(Bitmap b, int height)
    {
        float factor = height / (float) b.getHeight();
        return Bitmap.createScaledBitmap(b, (int) (b.getWidth() * factor), height, true);
    }

    // DeviceDimensionsHelper.convertPixelsToDp(25f, context) => (25px converted to dp)
    public static float convertPixelsToDp(float px, Context context){
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        float dp = px / (metrics.densityDpi / 160f);
        return dp;
    }
}
