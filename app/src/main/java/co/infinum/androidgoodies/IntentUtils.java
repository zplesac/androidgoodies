package co.infinum.androidgoodies;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.provider.MediaStore;

import java.text.DecimalFormat;
import java.util.Locale;

/**
 * Created by zeljkoplesac on 29/07/14.
 */
public class IntentUtils {

    private static final String SPACE = " ";

    private static final String SLASH = "/";

    /**
     * Prefix for the telephone <code>Uri</code>.
     */
    private static final String TEL_PREFIX = "tel:";

    /**
     * Template URL for building navigation <code>Uri</code>.
     */
    private static final String NAVIGATION_URL = "http://maps.google.com/maps?saddr=%s,%s&daddr=%s,%s";

    /**
     * MIME type of the content that is passed to the mail <code>Intent</code>.
     */
    private static final String MAIL_CONTENT_TYPE = "plain/text";

    /**
     * Builds an <code>Uri</code> that can be passed to <code>Intent</code> and start
     * navigation from current location to given latitude and longitude.
     *
     * @param currentLocation current location
     * @param latitude        latitude of the destination as decimal number
     * @param longitude       longitude of the destination as decimal number
     * @return <code>Uri</code> that can be passed to <code>Intent</code> in order to start navigation
     */
    public static Uri buildNavigationUri(Location currentLocation, double latitude, double longitude) {
        return Uri.parse(String.format(NAVIGATION_URL,
                formatDecimalNumber(currentLocation.getLatitude(), Locale.US),
                formatDecimalNumber(currentLocation.getLongitude(), Locale.US),
                formatDecimalNumber(latitude, Locale.US),
                formatDecimalNumber(longitude, Locale.US)));
    }

    /**
     * Parses given telephone number as <code>Uri</code>.
     *
     * @param phone telephone number
     * @return <code>Uri</code> parsed from given telephone number
     */
    public static Uri buildPhoneUri(String phone) {
        String uriTel = phone
                .replace(SPACE, "")
                .replace(SLASH, "");

        return Uri.parse(TEL_PREFIX + uriTel);
    }

    /**
     * Builds an <code>Intent</code> that can be used to start navigation from current location to
     * given latitude and longitude. Relies on {@link #buildNavigationUri(android.location.Location, double, double)}
     * to create navigation <code>Uri</code> for the intent.
     *
     * @param currentLocation current location
     * @param latitude        latitude of the destination as decimal number
     * @param longitude       longitude of the destination as decimal number
     * @return <code>Intent</code> that can be used to start navigation
     */
    public static Intent buildNavigationIntent(Location currentLocation, double latitude, double longitude) {
        return new Intent(Intent.ACTION_VIEW, buildNavigationUri(currentLocation, latitude, longitude));
    }

    /**
     * Formats the decimal number for the given locale.
     *
     * @param value decimal number to format
     * @param locale <code>Locale</code> to use for formatting
     * @return <code>String</code> representing given decimal number
     */
    private static String formatDecimalNumber(double value, Locale locale) {
        return DecimalFormat.getInstance(locale).format(value);
    }

    /**
     * Returns <code>true</code> if the device has telephony capability.
     *
     * @param context <code>Context</code> required to access telephony capability information
     * @return <code>true</code> if device has telephony capability
     */
    public static boolean hasTelephony(Context context) {
        PackageManager packageManager = context.getPackageManager();
        return packageManager.hasSystemFeature(PackageManager.FEATURE_TELEPHONY);
    }

    /**
     * Builds an <code>Intent</code> that can be used to start a call to the given number. Relies on
     * {@link #buildPhoneUri(String)} to create <code>Uri</code> for the intent.
     *
     * @param phone telephone number
     * @return <code>Intent</code> that can be used to start a call
     */
    public static Intent buildCallIntent(String phone) {
        return new Intent(Intent.ACTION_CALL, buildPhoneUri(phone));
    }

    /**
     * Builds an <code>Intent</code> that can be used to open mail application and start composing
     * mail to the given address.
     *
     * @param address email address of the receiver
     * @return <code>Intent</code> that can be used to start mail application
     */
    public static Intent buildMailIntent(String address) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType(MAIL_CONTENT_TYPE);
        intent.putExtra(Intent.EXTRA_EMAIL, new String[] { address });
        return intent;
    }

    /**
     * Builds an <code>Intent</code> that can be used to open browser application with the given
     * <code>URL</code> set.
     *
     * @param url <code>URL</code> that the browser will open
     * @return <code>Intent</code> that can be used to start the browser
     */
    public static Intent buildBrowserIntent(String url) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(url));
        return intent;
    }

    /**
     * Builds an <code>Intent</code> that can be used to open video application with the given
     * <code>URL</code> set.
     *
     * @param url <code>URL</code> that the video will open
     * @return <code>Intent</code> that can be used to start the browser
     */
    public static Intent buildVideoIntent(String url) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse(url));
        return intent;
    }

    /**
     * Builds an <code>Intent</code> that can be used to open camera app with the given output
     * file URI.
     * <code>URL</code> set.
     *
     * @param fileUri <code>Uri</code> where the image will be saved
     * @return <code>Intent</code> that can be used to open the camera
     */
    public static Intent buildCameraIntent(Uri fileUri) {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri); // set the image file name
        return intent;
    }

    /**
     * Builds an <code>Intent</code> that can be used to open gallery app.
     * <code>URL</code> set.
     *
     * @return <code>Intent</code> that can be used to open the gallery app.
     */
    public static Intent buildGalleryIntent() {
        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        return intent;
    }

    /**
     * Builds an <code>Intent</code> that can be used to open video application with the given
     * <code>URL</code> set.
     *
     * @param url <code>URL</code> that the video will open
     * @return <code>Intent</code> that can be used to start the browser
     */
    public static Intent buildImageIntent(String url) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setDataAndType(Uri.parse(url), "image/*");
        return intent;
    }

    /**
     * Builds an <code>Intent</code> that can be used to open Google play application with given
     * application
     *
     * @return <code>Intent</code> that can be used to start the Google Play
     */
    public static Intent buildGooglePlayIntent(){
        final String appPackageName = BuildConfig.APPLICATION_ID;
        try {
            return new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName));
        } catch (android.content.ActivityNotFoundException anfe) {
            return new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName));
        }
    }

    /**
     * Builds an <code>Intent</code> that can be used to start a phone app showing the given number. Relies on
     * {@link #buildPhoneUri(String)} to create <code>Uri</code> for the intent. Starting activity with this intent does not require
     * any of user's permissions, such as android.permission.CALL_PHONE
     *
     * @param phoneNumber telephone number
     * @return <code>Intent</code> that can be used to start a phone app
     */
    public static Intent buildPhoneIntent(String phoneNumber) {
        Intent intent = new Intent(Intent.ACTION_DIAL, buildPhoneUri(phoneNumber));
        return intent;
    }
}
