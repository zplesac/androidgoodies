package co.infinum.androidgoodies;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

/**
 * Created by zeljkoplesac on 04/02/15.
 */
public class KeyboardUtils {

    /**
     * Utility method which binds default Android keyboard to given view
     * @param mContext <code>Context</code> required to obtain system service
     * @param view <code>View</code> to which we bind the keyboard
     */
    public static void showKeyboard(Context mContext, View view){
        if(view == null){
            return;
        }

        InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(view, InputMethodManager.SHOW_FORCED);
    }

    /**
     * Utility method which unbinds default Android keyboard from given view
     * @param mContext <code>Context</code> required to obtain system service
     * @param view <code>View</code> from which we unbind the keyboard
     */
    public static void hideKeyboard(Context mContext, View view){
        if(view == null){
            return;
        }

        InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    /**
     * Utility method which unbinds default Android keyboard from the given activity.
     * @param activity <code>Activity</code> from which we want to unbind the keyboard
     */
    public static void hideKeyboard(Activity activity){
        if(activity.isFinishing() || activity.getCurrentFocus() == null){
            return;
        }

        InputMethodManager inputMethodManager = (InputMethodManager) activity
                .getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus()
                .getWindowToken(), 0);
    }
}
