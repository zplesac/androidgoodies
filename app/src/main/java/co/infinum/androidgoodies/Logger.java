package co.infinum.androidgoodies;

import android.util.Log;

/**
 * Created by zeljkoplesac on 14/02/15.
 */
public class Logger {

    private static enum LogVariant { DEBUG, INFO, ERROR};

    private static String TAG = "LOGGER";

    private static boolean loggingEnabled = true;

    private static void log(String tag, String msg, LogVariant variant) {

        if(loggingEnabled){
            try{

                switch (variant){
                    case DEBUG:
                        Log.d(tag, msg);
                        break;
                    case INFO:
                        Log.i(tag, msg);
                        break;
                    case ERROR:
                        Log.e(tag, msg);
                        break;
                }

            }
            catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    public static void d(String msg) {
        log(TAG, msg, LogVariant.DEBUG);
    }

    public static void d(String tag, String msg) {
        log(tag, msg, LogVariant.DEBUG);
    }

    public static void e(String msg){
        log(TAG, msg, LogVariant.ERROR);
    }

    public static void e(String tag, String msg){
        log(tag, msg, LogVariant.ERROR);
    }

    public static void i(String msg){
        log(TAG, msg, LogVariant.INFO);
    }

    public static void i(String tag, String msg){
        log(tag, msg, LogVariant.INFO);
    }

    public static String getTag() {
        return TAG;
    }

    public static void setTag(String TAG) {
        Logger.TAG = TAG;
    }

    public static boolean isLoggingEnabled() {
        return loggingEnabled;
    }

    public static void setLoggingEnabled(boolean loggingEnabled) {
        Logger.loggingEnabled = loggingEnabled;
    }
}
